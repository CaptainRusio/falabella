# falabella

Prerrequisitos:
 - Python 3.6 o superior.
 - Flask (Para instalar flask, usar el siguiente comando: pip install flask)

Levantar API: 
 - Abrir consola en la carpeta clonada.
 - usar el siguiente comando: python main.py

Probar test unitarios:
 - Abrir consola en la carpeta clonada.
 - usar el siguiente comando: python tests.py