'''Created by Enrique Avilés
   Last modification 08/09/2021'''

from flask import Flask, jsonify, request
from math import pi
import fibonacci

app = Flask(__name__)


@app.route('/pi/<int:n>', methods=['GET'])
def getPi(n):
    response = None
    # Limits of N between 0 and 12
    if n >= 0 and n <= 12:
        m = fibonacci.fib(n)
        pifibonacci = round(pi, m)
        ndecimal = str(pifibonacci).split(".")
        if len(ndecimal[1]) > n-1:
            ndecimal = ndecimal[1][n-1]
        else:
            ndecimal = -1
        response = [
            {"pi-fibonacci": str(pifibonacci),
             "n-decimal": str(ndecimal)}
        ]
    # Exceptions handling
    if response is not None:
        return jsonify(response), 200
    else:
        return "Pi not found", 404

if __name__ == "__main__":
    app.run(debug=True)