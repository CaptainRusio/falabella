'''Created by Enrique Avilés
   Last modification 08/09/2021'''

import fibonacci
import unittest

class UnitTest(unittest.TestCase):

    def testFibonacciOne(self):
        self.assertTrue(fibonacci.fib(0) == 0)

    def testFibonacciTwo(self):
        self.assertTrue(fibonacci.fib(2) == 1)

    def testFibonacciThree(self):
        self.assertTrue(fibonacci.fib(3) == 2)

    def testFibonacciFour(self):
        self.assertTrue(fibonacci.fib(4) == 3)


if __name__ == "__main__":
    unittest.main()
