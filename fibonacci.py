'''Created by Enrique Avilés
   Last modification 08/09/2021'''

def fib(n):
    if n >= 0 and n <= 12:
        a = 0
        b = 1
        for k in range(n):
            c = b+a
            a = b
            b = c
        return a
    else:
        return n